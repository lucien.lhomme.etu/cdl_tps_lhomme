import yaml
import os
import argparse

FICHIER_DE_CONFIGURATION="config.yaml"

parser = argparse.ArgumentParser()
parser.add_argument('--configuration', 
                    type=argparse.FileType('r', encoding='UTF-8'), 
                    required=False)
args = parser.parse_args()

if args.configuration:
    filename = args.configuration.name
elif os.environ["FICHIER_DE_CONFIGURATION"]:
    filename = os.environ["FICHIER_DE_CONFIGURATION"]
else:
    filename=FICHIER_DE_CONFIGURATION

with open(filename) as file:
    config = yaml.safe_load(file)

    user = config["user"]
    print("user: ")
    print("name: ", user["name"])
    print("password: ", user["password"])